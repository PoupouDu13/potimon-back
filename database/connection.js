const Sequelize = require('sequelize');
const sequelize = new Sequelize(
    process.env.MYSQL_DATABASE || 'potimondb',
    process.env.MYSQL_USER || 'root',
    process.env.MYSQL_PASSWORD || 'password',
    {
        host: process.env.MYSQL_SERVER || 'localhost',
        dialect: 'mysql',
        operatorsAliases: false,
    });

module.exports = sequelize;