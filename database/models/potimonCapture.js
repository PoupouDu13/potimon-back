const Sequelize = require('sequelize');
const sequelize = require('../connection');

const PotimonCapture = sequelize.define('potimonCapture', {
    potidex_id: {
        type: Sequelize.INTEGER
    },
});

sequelize.sync();

module.exports = PotimonCapture;