const Sequelize = require('sequelize');
const sequelize = require('../connection');

const PotimonReserve = sequelize.define('potimonReserve', {
    potidex_id: {
        type: Sequelize.INTEGER
    },
    game_id: {
        type: Sequelize.STRING
    },
    level: {
        type: Sequelize.INTEGER
    },
    current_hp: {
        type: Sequelize.INTEGER
    },
    current_mana: {
        type: Sequelize.INTEGER
    },
    experience: {
        type: Sequelize.INTEGER
    },
});

sequelize.sync();

module.exports = PotimonReserve;