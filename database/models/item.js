const Sequelize = require('sequelize');
const sequelize = require('../connection');

const Item = sequelize.define('item', {
    quantity: {
        type: Sequelize.INTEGER
    },
    name: {
        type: Sequelize.STRING
    },
});

sequelize.sync();

module.exports = Item;