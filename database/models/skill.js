const Sequelize = require('sequelize');
const sequelize = require('../connection');

const Skill = sequelize.define('skill', {
    skill_id: {
        type: Sequelize.INTEGER
    },
});

sequelize.sync();

module.exports = Skill;