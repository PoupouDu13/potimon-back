const Sequelize = require('sequelize');
const sequelize = require('../connection');
const Potimon = require('./potimon');
const PotimonReserve = require('./potimonReserve');
const Item = require('./item');

const User = sequelize.define('user', {
    username: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING
    },
    password: {
        type: Sequelize.STRING
    },
    guid_token: {
        type: Sequelize.STRING
    },
    active: {
        type: Sequelize.BOOLEAN, defaultValue: false
    },
    session_guid: {
        type: Sequelize.STRING
    },
});

User.hasMany(Potimon, { as: 'potimons' });
User.hasMany(PotimonReserve, { as: 'potimonReserves' });
User.hasMany(Item, { as: 'items' });

sequelize.sync();

module.exports = User;