const Sequelize = require('sequelize');
const sequelize = require('../connection');
const Skill = require('./skill');

const Potimon = sequelize.define('potimon', {
    potidex_id: {
        type: Sequelize.INTEGER
    },
    game_id: {
        type: Sequelize.STRING
    },
    level: {
        type: Sequelize.INTEGER
    },
    current_hp: {
        type: Sequelize.INTEGER
    },
    current_mana: {
        type: Sequelize.INTEGER
    },
    experience: {
        type: Sequelize.INTEGER
    },
});

Potimon.hasMany(Skill);

sequelize.sync();

module.exports = Potimon;