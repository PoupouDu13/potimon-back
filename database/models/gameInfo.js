const Sequelize = require('sequelize');
const sequelize = require('../connection');
const User = require('./user');
const PotimonCapture = require('./potimonCapture');

const GameInfo = sequelize.define('gameInfo', {
    in_game_time: {
        type: Sequelize.STRING
    },
    current_carte_id: {
        type: Sequelize.INTEGER
    },
    potiflouz: {
        type: Sequelize.INTEGER
    },
});

GameInfo.belongsTo(User);
GameInfo.hasMany(PotimonCapture, {as: 'potimonCaptures'});

sequelize.sync();

module.exports = GameInfo;