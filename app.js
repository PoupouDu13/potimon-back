const express = require('express');
const app = express();
const http = require('http').Server(app);
const cors = require('cors');
const bodyParser = require('body-parser');

const api = require('./api/routes.js');
const sequelize = require('./database/connection');
const user = require('./database/models/user');
const potimon = require('./database/models/potimon');
const potimonReserve = require('./database/models/potimonReserve');
const gameInfo = require('./database/models/gameInfo');
const potimonCapture = require('./database/models/potimonCapture');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
app.options('*', cors());

app.use('/api', api);

sequelize.authenticate()
  .then(async () => {
    console.log('Connection has been established successfully.');
    const users = await user.findAll();
    console.log(users);
    const potimons = await potimon.findAll()
    console.log(potimons);
    const gameInfos = await gameInfo.findAll()
    console.log(gameInfos);
    const potimonReserves = await potimonReserve.findAll()
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

const port = process.env.ALWAYSDATA_HTTPD_PORT || 4000;
const ip = process.env.ALWAYSDATA_HTTPD_IP || '0.0.0.0';
http.listen(port, ip, function(){
    console.log('listening on *:' + port);
});
