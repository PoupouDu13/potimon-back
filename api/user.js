const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();
const User = require('../database/models/user.js');
const Helper = require('../helpers/helper');
const JWT_SECRET = require('../constants').JWT_SECRET;

router.post('/insertUser', function (req, res) {
    User.create({
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
    }).then(function (result) {
        res.json(result);
    }).catch(function (error) {
        res.status(500).send("Pseudo ou email non valide/existant.")
    });
});

router.post('/findUser', function (req, res) {
    User.findAll({
        where: {
            username: req.body.username,
            password: req.body.password,
        }
    }).then(function (result) {
        res.json(result);
    }).catch(function (error) {
        res.status(500).send("Pseudo ou email non valide/existant.")
    });
});

router.post('/login', function (req, res) {
    User.findOne({
        where: {
            username: req.body.username,
            password: req.body.password,
        }
    }).then(function (result) {
        if(!result){
            return res.status(400).json({
                message: 'Bad credentials'
            })
        }

        const {username, email} = result;
        const token = jwt.sign({
            username,
            email
        }, JWT_SECRET);

        res.json({
            username,
            email,
            token
        })
    }).catch(function (error) {
        res.status(500).send("Pseudo ou email non valide/existant.")
    });
});

module.exports = router;