const express = require('express');
const router = express.Router();
const Potimon = require('../database/models/potimon.js');
const Helper = require('../helpers/helper');

router.get('/findPotimon', function (req, res) {
    Potimon.findAll({
        where: {
            id: req.query.id
        }
    }).then(function (result) {
        res.json(result);
    }).catch(function (error) {
        res.status(500).send("Pseudo ou email non valide/existant.")
    });
});

module.exports = router;