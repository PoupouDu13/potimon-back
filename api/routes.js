const express = require('express');
const jwt = require('express-jwt');
const router = express.Router();
const user = require('./user.js');
const potimon = require('./potimon.js');
const selectPotimon = require('./selectPotimon');

const JWT_SECRET = require('../constants').JWT_SECRET;


router.use('/user', user);
router.use('/potimon',
    jwt({ secret: JWT_SECRET }),
    potimon);
router.use('/selectPotimon',
    jwt({ secret: JWT_SECRET }), selectPotimon);

module.exports = router;